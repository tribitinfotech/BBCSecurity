//
//  InitialViewController.swift
//  Hirego
//
//  Created by Jitendra bhadja on 18/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnLogin.layer.cornerRadius = 5.0
        self.btnRegister.layer.cornerRadius = 5.0
        
        print(UserDefaults.standard.value(forKey: "userId") as? String ?? "")
        
        if UserDefaults.standard.value(forKey: "userId") as? String != "" {
            
            let obj:HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(obj, animated: false)
            
        }
        
        // Do any additional setup after loading the view.
    }

    
    
    //MARK:- Action zone
    @IBAction func btnLoginAction(_ sender: Any)
    {
        let obj:LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnRegisterAction(_ sender: Any)
    {
       
    }
    
    
    //MARK:- Memory managment method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
