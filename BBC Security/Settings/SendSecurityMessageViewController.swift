//
//  SendSecurityMessageViewController.swift
//  BBC Security
//
//  Created by Jitendra bhadja on 28/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView
class SendSecurityMessageViewController: UIViewController,UITextViewDelegate,UIAlertViewDelegate {

    @IBOutlet weak var btnSend: UIButton!
    
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtPlaceHolder: UITextField!
    
    var dictUser:NSMutableDictionary = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let dataProfile:NSData = UserDefaults.standard.object(forKey: "userData") as! NSData
        
        self.dictUser =  NSMutableDictionary.init(dictionary: NSKeyedUnarchiver.unarchiveObject(with: dataProfile as Data as Data) as! NSDictionary)
        
        print(self.dictUser)
        
        SKActivityIndicator.spinnerColor(UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0))
        SKActivityIndicator.statusTextColor(UIColor.black)
        let myFont = UIFont(name: "AvenirNext-DemiBold", size: 18)
        SKActivityIndicator.statusLabelFont(myFont!)
        
        self.txtDescription.layer.cornerRadius = 2.0
       
        self.btnSend.layer.cornerRadius = 2.0
        
        self.txtDescription.layer.borderWidth = 1.0
        self.txtDescription.layer.borderColor = UIColor.init(red: 54.0/255.0, green: 119.0/255.0, blue: 108.0/255.0, alpha: 1.0).cgColor
        
        // Do any additional setup after loading the view.
    }

    //MARK:- Textview delegate
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.txtPlaceHolder.text = textView.text
    }
    
    //MARK:- Action zones
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSendAction(_ sender: Any)
    {
        
        if self.txtDescription.text == "" {
            Alert.show("Please enter message to send", self)
            return
        }
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = "https://active-bbc-dev.azurewebsites.net/api/imsafeadmin"
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "POST"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(self.txtDescription.text, forKey: "message")
        parameter.setValue(UserDefaults.standard.value(forKey: "customerId") as? String, forKey: "customerId")
        
        print(parameter)
        
        var jsondata2 = Data()
        
        do {
            jsondata2 = try JSONSerialization.data(withJSONObject: parameter, options: JSONSerialization.WritingOptions.prettyPrinted)
            // use jsonData
        } catch {
            // report error
        }
        
        request.httpBody = jsondata2
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let strResponse:String = String(decoding: data, as: UTF8.self)
                    
                    print(strResponse)
                    
                    SKActivityIndicator.dismiss()
                   
                    if strResponse.contains("Success") == true
                    {
                        let alert = UIAlertController(title: Title, message: "Send Successfully", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                            DispatchQueue.main.async {
                                self.navigationController?.popViewController(animated: true)
                            }
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
    
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    
  
    
    //MARK:- Memory managment method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
