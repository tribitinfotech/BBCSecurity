    //
//  SecurityListViewController.swift
//  BBC Security
//
//  Created by Jitendra bhadja on 29/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView

class SecurityListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblMessages: UITableView!
    var arrSecurityMessages:NSMutableArray = NSMutableArray()
    var dictUser:NSMutableDictionary = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let dataProfile:NSData = UserDefaults.standard.object(forKey: "userData") as! NSData
        
        self.dictUser =  NSMutableDictionary.init(dictionary: NSKeyedUnarchiver.unarchiveObject(with: dataProfile as Data as Data) as! NSDictionary)
        
        print(self.dictUser)
        
        SKActivityIndicator.spinnerColor(UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0))
        SKActivityIndicator.statusTextColor(UIColor.black)
        let myFont = UIFont(name: "AvenirNext-DemiBold", size: 18)
        SKActivityIndicator.statusLabelFont(myFont!)
        
        self.getSecurityMessages()
        // Do any additional setup after loading the view.
    }

    
    //MARK:- Tableview delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrSecurityMessages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell  = self.tblMessages.dequeueReusableCell(withIdentifier: "cellAids") as! cellAids
        
        let dict:NSMutableDictionary = (self.arrSecurityMessages.object(at: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        
       
                
        if let safeStatus = dict.value(forKey: "ImSafe") as? NSInteger {
            
            if safeStatus == 0
            {
                 cell.lblTitle.text = "\(dict.value(forKey: "FullName") as! String) needs help"
                
                cell.lblTitle.textColor = UIColor.red
            }
            else
            {
                 cell.lblTitle.text = "\(dict.value(forKey: "FullName") as! String)"
                
                cell.lblTitle.textColor = UIColor.black
            }
        }
        
        if let strDate = dict.value(forKey: "CreatedAt") as? String {
            
            let dateFormatter = DateFormatter()
            let tempLocale = dateFormatter.locale // save locale temporarily
            dateFormatter.locale = Locale.current // set locale to reliable US_POSIX
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssX"
            let date = dateFormatter.date(from: strDate)!
            dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
            dateFormatter.locale = tempLocale // reset the locale
            let dateString = dateFormatter.string(from: date)
            print("EXACT_DATE : \(dateString)")
            
            cell.lblDate.text = dateString
        }
        
        
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    //MARK:- Action zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Private message
    
    func getSecurityMessages()
    {
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
     
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"

        let result = formatter.string(from: date)

        
        let strURL:String = "https://active-bbc-dev.azurewebsites.net/api/imsafeadmin?customerId=\(String(describing: (UserDefaults.standard.value(forKey: "customerId") as! String)))&dtCheck=\(result)"
        
        print(strURL)
        
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let arrResult = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    print(arrResult)
                    
                    self.arrSecurityMessages = arrResult
                    
                    self.tblMessages.reloadData()
                    
                    
                    SKActivityIndicator.dismiss()
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
