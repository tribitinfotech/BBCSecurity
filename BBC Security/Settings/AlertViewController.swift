//
//  AlertViewController.swift
//  BBC Security
//
//  Created by Jitendra bhadja on 30/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView

class AlertViewController: UIViewController,UIAlertViewDelegate {

    @IBOutlet weak var lblMessage: UILabel!
    
    var dict:NSMutableDictionary?
    
    @IBOutlet weak var lblBg: UILabel!
    
    var dictUser:NSMutableDictionary = NSMutableDictionary()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        DispatchQueue.main.async {
//            self.navigationController?.popViewController(animated: true)
//        }
        
        SKActivityIndicator.spinnerColor(UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0))
        SKActivityIndicator.statusTextColor(UIColor.black)
        let myFont = UIFont(name: "AvenirNext-DemiBold", size: 18)
        SKActivityIndicator.statusLabelFont(myFont!)
        
        self.lblMessage.text = self.dict?.value(forKey: "Message") as? String
        
        self.lblBg.layer.cornerRadius = 5.0
        
        self.lblBg.clipsToBounds = true
        
        let dataProfile:NSData = UserDefaults.standard.object(forKey: "userData") as! NSData
        
        self.dictUser =  NSMutableDictionary.init(dictionary: NSKeyedUnarchiver.unarchiveObject(with: dataProfile as Data as Data) as! NSDictionary)
        
        print(self.dictUser)
        // Do any additional setup after loading the view.
    }
    func postIMSafeAdmin(status:String)
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = "https://active-bbc-dev.azurewebsites.net/api/imsafeadmin"
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "POST"
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("21baedf6-70c1-459e-b016-f362eb8469eb", forHTTPHeaderField: "Postman-Token")
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(self.dict?.value(forKey: "Id"), forKey: "imsafeMessageId")
        parameter.setValue(status, forKey: "imSafe")
        
        print(parameter)
        
        var jsondata2 = Data()
        
        do {
            jsondata2 = try JSONSerialization.data(withJSONObject: parameter, options: JSONSerialization.WritingOptions.prettyPrinted)
            // use jsonData
        } catch {
            // report error
        }
        
        request.httpBody = jsondata2
        
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                SKActivityIndicator.dismiss()
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let strResponse:String = String(decoding: data, as: UTF8.self)
                    
                    print(strResponse)
                    
                    SKActivityIndicator.dismiss()
                    
                    if strResponse.contains("Success") == true
                    {
                        if status == "true"
                        {
                            let alert = UIAlertController(title: Title, message: "We’re glad you’re safe. Thanks for letting us know", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                                DispatchQueue.main.async {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    func postIMSafe(status:String)
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = "https://active-bbc-dev.azurewebsites.net/api/imsafe"
        
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "POST"
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("21baedf6-70c1-459e-b016-f362eb8469eb", forHTTPHeaderField: "Postman-Token")
  
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(self.dict?.value(forKey: "Id"), forKey: "imsafeMessageId")
        parameter.setValue(status, forKey: "imSafe")
        
        print(parameter)
        
        var jsondata2 = Data()
        
        do {
            jsondata2 = try JSONSerialization.data(withJSONObject: parameter, options: JSONSerialization.WritingOptions.prettyPrinted)
            // use jsonData
        } catch {
            // report error
        }
        
        request.httpBody = jsondata2
        
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let strResponse:String = String(decoding: data, as: UTF8.self)
                    
                    print(strResponse)
                    
                    SKActivityIndicator.dismiss()
                    
                    if strResponse.contains("Success") == true
                    {
                        if status == "true"
                        {
                            let alert = UIAlertController(title: Title, message: "We’re glad you’re safe. Thanks for letting us know", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                                DispatchQueue.main.async {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            let alert = UIAlertController(title: Title, message: "We have received your request for help and our security staff will be in contact shortly", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                                DispatchQueue.main.async {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnIamSafeAction(_ sender: Any)
    {
        if UserDefaults.standard.bool(forKey: "isAdmin") == true {
            
            //admin safe
            
            self.postIMSafe(status: "true")
            
        }
        else
        {
            self.postIMSafe(status: "true")
            //normal user safe
        }
    }
    
    @IBAction func btnHelpAction(_ sender: Any)
    {
        if UserDefaults.standard.bool(forKey: "isAdmin") == true {
            
            //admin safe
            self.postIMSafe(status: "false")
            
        }
        else
        {
            self.postIMSafe(status: "false")
            //normal user safe
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
