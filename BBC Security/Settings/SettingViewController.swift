//
//  SettingViewController.swift
//  BBC Security
//
//  Created by Jitendra bhadja on 26/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var topSpaceLogout: NSLayoutConstraint!
    @IBOutlet weak var heightSafeResponses: NSLayoutConstraint!
    @IBOutlet weak var heightSecurityAlert: NSLayoutConstraint!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnSafe: UIButton!
    @IBOutlet weak var btnSecurity: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaults.standard.bool(forKey: "isAdmin") == true {
            
            self.btnSecurity.isHidden = false
            self.btnSafe.isHidden = false
            
           self.topSpaceLogout.constant = 33.0
            self.heightSafeResponses.constant = 55.0
            
            self.heightSecurityAlert.constant = 55.0
        }
        else
        {
            self.btnSecurity.isHidden = true
            self.btnSafe.isHidden = true
            
           self.topSpaceLogout.constant = 0.0
            self.heightSafeResponses.constant = 0.0
            
            self.heightSecurityAlert.constant = 0.0
        }
        
        self.btnSecurity.layer.cornerRadius = 5.0
        
        self.btnSafe.layer.cornerRadius = 5.0
        
        self.btnLogout.layer.cornerRadius = 5.0
        
        // Do any additional setup after loading the view.
    }

    //MARK:- Action zones
    
    @IBAction func btnResponsesAction(_ sender: Any)
    {
        let obj:SecurityListViewController = self.storyboard?.instantiateViewController(withIdentifier: "SecurityListViewController") as! SecurityListViewController
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnSecurityAction(_ sender: Any)
    {
        let obj:SendSecurityMessageViewController = self.storyboard?.instantiateViewController(withIdentifier: "SendSecurityMessageViewController") as! SendSecurityMessageViewController
        self.navigationController?.pushViewController(obj, animated: false)
    }
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnLogoutAction(_ sender: Any)
    {
        for nav in (self.navigationController?.viewControllers)!
        {
            print(nav)
            if nav.isKind(of: InitialViewController.self)
            {
                self.navigationController?.popToViewController(nav, animated: false)
                
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
