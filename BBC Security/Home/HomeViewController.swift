//
//  HomeViewController.swift
//  BBC Security
//
//  Created by Jitendra bhadja on 26/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView

class CellMenu:UICollectionViewCell
{
    @IBOutlet weak var imgMenu: UIImageView!
    
    @IBOutlet weak var lblMenu: UILabel!
}
class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var clctnHome: UICollectionView!
    
    var dictUser:NSMutableDictionary = NSMutableDictionary()
    var arrMenu:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SKActivityIndicator.spinnerColor(UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0))
        SKActivityIndicator.statusTextColor(UIColor.black)
        let myFont = UIFont(name: "AvenirNext-DemiBold", size: 18)
        SKActivityIndicator.statusLabelFont(myFont!)
        
        let dataProfile:NSData = UserDefaults.standard.object(forKey: "userData") as! NSData
        
        self.dictUser =  NSMutableDictionary.init(dictionary: NSKeyedUnarchiver.unarchiveObject(with: dataProfile as Data as Data) as! NSDictionary)
        
        print(self.dictUser)
        
        let dictQHSC:NSMutableDictionary = NSMutableDictionary()
        dictQHSC.setValue("QHSE Docs", forKey: "title")
        dictQHSC.setValue("ic_qhsc.png", forKey: "image")
        self.arrMenu.add(dictQHSC)
        
        let dictTool:NSMutableDictionary = NSMutableDictionary()
        dictTool.setValue("Toolbox Talks", forKey: "title")
        dictTool.setValue("ic_toolbox.png", forKey: "image")
        self.arrMenu.add(dictTool)
        
        let dictFirstAid:NSMutableDictionary = NSMutableDictionary()
        dictFirstAid.setValue("First Aider", forKey: "title")
        dictFirstAid.setValue("ic_first_aid.png", forKey: "image")
        self.arrMenu.add(dictFirstAid)
        
        let dictSite:NSMutableDictionary = NSMutableDictionary()
        dictSite.setValue("Site Fire", forKey: "title")
        dictSite.setValue("ic_site_fire.png", forKey: "image")
        self.arrMenu.add(dictSite)
        
        let dictMethod:NSMutableDictionary = NSMutableDictionary()
        dictMethod.setValue("Method Statements", forKey: "title")
        dictMethod.setValue("ic_method_statement.png", forKey: "image")
        self.arrMenu.add(dictMethod)
        
        /*let dictHelp:NSMutableDictionary = NSMutableDictionary()
        dictHelp.setValue("I Need Help", forKey: "title")
        dictHelp.setValue("ic_help.png", forKey: "image")
        self.arrMenu.add(dictHelp)*/
        
        /*let dictSafe:NSMutableDictionary = NSMutableDictionary()
        dictSafe.setValue("I'm Safe", forKey: "title")
        dictSafe.setValue("ic_shield.png", forKey: "image")
        self.arrMenu.add(dictSafe)*/
        
        self.clctnHome.reloadData()
        
        self.checkRole()
        
        // Do any additional setup after loading the view.
    }

    //MARK:- Private Merhod
    func postIMSafe()
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = iMsafe
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "POST"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let strResponse:String = String(decoding: data, as: UTF8.self)
                    
                    print(strResponse)
                    
                    SKActivityIndicator.dismiss()
                    
                    if strResponse.contains("Success") == true
                    {
                       Alert.show("We’re glad you’re safe. Thanks for letting us know", self)
                    }
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    func checkRole()
    {
        
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        let strURL:String = MainDomain + checkRoles
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("32892de6-aae6-c54e-adf4-1ecf9589b6c8", forHTTPHeaderField: "Postman-Token")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let arrResult = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    print(arrResult)
                    
                    if arrResult.count != 0
                    {
                        let dictAdmin:NSMutableDictionary = arrResult.object(at: 0) as! NSMutableDictionary
                        
                        if dictAdmin.value(forKey: "Name") as! String == "Admin"
                        {
                            UserDefaults.standard.set(true, forKey: "isAdmin")
                            UserDefaults.standard.set(dictAdmin.value(forKey: "Id"), forKey: "adminId")
                            UserDefaults.standard.synchronize()
                        }
                        else
                        {
                            UserDefaults.standard.set(false, forKey: "isAdmin")
                            UserDefaults.standard.synchronize()
                        }
                        
                        
                    }
                    else
                    {
                        UserDefaults.standard.set(false, forKey: "isAdmin")
                        UserDefaults.standard.synchronize()
                    }
                    
                    self.getCustomer()
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    func getMessage()
    {
        
        let strURL:String = "https://active-bbc-dev.azurewebsites.net/api/imsafe?customerid=" + (UserDefaults.standard.value(forKey: "customerId") as? String)!
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         request.addValue("6705cb61-1bc2-4114-b255-40dc260e0961", forHTTPHeaderField: "Postman-Token")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let arrResult = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    print(arrResult)
                    
                    if arrResult.count != 0
                    {
                        let dictAdmin:NSMutableDictionary = arrResult.object(at: 0) as! NSMutableDictionary
                        
                        let obj:AlertViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
                        obj.dict = dictAdmin
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    
                    SKActivityIndicator.dismiss()
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    func getCustomer()
    {

        let strURL:String = customers
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let arrResult = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    print(arrResult)

                    if arrResult.count != 0
                    {
                        let dictAdmin:NSMutableDictionary = arrResult.object(at: 0) as! NSMutableDictionary
                        
                        UserDefaults.standard.set(dictAdmin.value(forKey: "Id"), forKey: "customerId")
                        UserDefaults.standard.synchronize()
                    }
                  
                    
                    self.getMessage()
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    //MARK:- Action zone
    
    @IBAction func btnSettingAction(_ sender: Any)
    {
        let obj:SettingViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    //MARK:- Collectionview delegate
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrMenu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellMenu", for: indexPath) as! CellMenu
        
        let dict:NSMutableDictionary = self.arrMenu.object(at: indexPath.row) as! NSMutableDictionary
        
        cell.lblMenu.text = dict.value(forKey: "title") as? String
        
        cell.imgMenu.image = UIImage.init(named: dict.value(forKey: "image") as! String)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // Compute the dimension of a cell for an NxN layout with space S between
        // cells.  Take the collection view's width, subtract (N-1)*S points for
        // the spaces between the cells, and then divide by N to find the final
        // dimension for the cell's width and height.
        
        let cellsAcross: CGFloat = 3
        let spaceBetweenCells: CGFloat = 0
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        return CGSize(width: dim, height: 160)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict:NSMutableDictionary = self.arrMenu.object(at: indexPath.row) as! NSMutableDictionary
        
        if indexPath.row == 2 {
            
            let obj:DocumentViewController = self.storyboard?.instantiateViewController(withIdentifier: "DocumentViewController") as! DocumentViewController
            obj.isFromFirstAiders = true
            obj.strTitle = (dict.value(forKey: "title") as? String)!
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else if indexPath.row == 6
        {
            self.postIMSafe()
        }
        else
        {
            let obj:DocumentViewController = self.storyboard?.instantiateViewController(withIdentifier: "DocumentViewController") as! DocumentViewController
            obj.isFromFirstAiders = false
            obj.strTitle = (dict.value(forKey: "title") as? String)!
            
            if indexPath.row == 0
            {
                obj.strDocType = "QHSE"
                self.navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 1
            {
                obj.strDocType = "TB"
                self.navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 4
            {
                obj.strDocType = "MS"
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
