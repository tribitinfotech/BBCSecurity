//
//  BasicStuff.swift
//  GameON
//
//  Created by JD on 14/06/17.
//  Copyright © 2017 JDB Techs. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import Alamofire


let Key_DeviceToken = "device_token"

func JKBLog(_ logMessage: Any, functionName: String = #function ,file:String = #file,line:Int = #line) {
    print("\(((file as NSString).lastPathComponent as NSString).deletingPathExtension)-->\(functionName),Line:\(line) ==> \(logMessage)")
}

struct Token {
    
    static let `default` = { () -> String in 
        if Default.value(forKey: Key_DeviceToken) != nil {
            return Default.value(forKey: Key_DeviceToken) as! String
        }
        else {
            return "Not Found"
        }
    }
   static var token = "Not Found" {
        didSet {
            Default.setValue(token, forKey: Key_DeviceToken)
            Default.synchronize()
        }
    }
}

class BasicStuff: NSObject {
    static let shared:BasicStuff = BasicStuff()
    
    class func getVenderID() -> String {
        return UIDevice.current.identifierForVendor!.uuidString
    }
}
class HUD {
    class func showHUD(_ view:UIView , text:String? = "" , mode:MBProgressHUDMode? = .indeterminate, style:MBProgressHUDBackgroundStyle? = .solidColor) {
        let progressHUD = MBProgressHUD()
        progressHUD.bezelView.color = .white
        
        if #available(iOS 9.0, *) {
            UIActivityIndicatorView.appearance(whenContainedInInstancesOf: [MBProgressHUD.self]).color = UIColor.init(red: 236.0/255.0, green: 204.0/255.0, blue: 119.0/255.0, alpha: 1.0)
        } else {
            // Fallback on earlier versions
            progressHUD.activityIndicatorColor = UIColor.init(red: 236.0/255.0, green: 204.0/255.0, blue: 119.0/255.0, alpha: 1.0)
        }
        progressHUD.backgroundView.style = style!
        progressHUD.mode = mode!
        progressHUD.animationType = .fade
        if text != nil {
            progressHUD.label.text = text
        }
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    class func hideHUD(_ view:UIView) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
class Alert {
    class func show(_ message:String,_ controller:UIViewController) {
        let alert = UIAlertController(title: Title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
            
        }))
        controller.present(alert, animated: true, completion: nil)
    }
}

extension UIColor {
    
    convenience init(hexString:String) {
        let hexString:String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        
        return NSString(format:"#%06x", rgb) as String
    }
    
}

extension String {
    
   
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound - r.lowerBound)
        return String(self[Range(start ..< end)])
    }
    func isValidEmail() -> Bool  {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        //let emailRegEx = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}

extension UIStoryboard {
    class func getController(_ identifier:String) -> UIViewController {
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
}
extension UIView {
    func makeCornerRadius(radius: CGFloat) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
    }
    func makeBoarder(color: UIColor , width: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    func copyView() -> UIView {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self))! as! UIView
    }
    func takeSnap()-> UIImage {
        if UIScreen.main.responds(to: #selector(getter: UIScreen.main.scale)) {
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        }
        else {
            UIGraphicsBeginImageContext(self.bounds.size)
        }
        
        self.window!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    
    /**
     Rounds the given set of corners to the specified radius
     
     - parameter corners: Corners to round
     - parameter radius:  Radius to round to
     */
    func round(corners: UIRectCorner, radius: CGFloat) {
        _round(corners: corners, radius: radius)
    }
    
    /**
     Rounds the given set of corners to the specified radius with a border
     
     - parameter corners:     Corners to round
     - parameter radius:      Radius to round to
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func round(corners: UIRectCorner, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        let mask = _round(corners: corners, radius: radius)
        addBorder(mask: mask, borderColor: borderColor, borderWidth: borderWidth)
    }
}

private extension UIView {
    
    @discardableResult func _round(corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.layer.masksToBounds = true
        return mask
    }
    
    func addBorder(mask: CAShapeLayer, borderColor: UIColor, borderWidth: CGFloat) {
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = bounds
        layer.addSublayer(borderLayer)
    }
    
}

class BadgeView: UIView {
    @IBOutlet weak var badgeLabel: UILabel!
    
    func setBadgeValue(_ value:Int) {
        if value <= 0 {
            self.isHidden = true
        }
        else {
            self.isHidden = false
            self.badgeLabel.text = "\(value)"
        }
    }
}
