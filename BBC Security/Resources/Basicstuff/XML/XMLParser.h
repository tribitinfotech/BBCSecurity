//
//  XMLParser.h
//  LaneCove
//
//  Created by New MacMini on 21/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface XMLParser : NSObject<NSXMLParserDelegate>
{
    NSMutableDictionary *dictEvent;
    NSMutableString *currentElementValue;    
}
@property (nonatomic, retain) NSMutableDictionary *dictEvent;

- (XMLParser *) initXMLParser;
@end
