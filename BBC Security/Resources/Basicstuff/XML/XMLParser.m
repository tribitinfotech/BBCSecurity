//
//  XMLParser.m
//  LaneCove
//
//  Created by New MacMini on 21/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
/*
<GetOfferInBusinessResponse xmlns="http://tempuri.org/">
<>
<>

*/

#import "XMLParser.h"


@implementation XMLParser

@synthesize dictEvent;

- (XMLParser *) initXMLParser {
	
	return self;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 
	attributes:(NSDictionary *)attributeDict {
	
    NSLog(@"elementName:%@",elementName);
    
	if([elementName isEqualToString:@"channel"]) 
    {
		
	}
  	else if([elementName isEqualToString:@"item"])
	{
		if (dictEvent == nil) {
			dictEvent = [[NSMutableDictionary alloc] init];
		}
		
	}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string { 
	
	if(!currentElementValue) 
		currentElementValue = [[NSMutableString alloc] initWithString:string];
	else
		[currentElementValue appendString:string];
	
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	if([elementName isEqualToString:@"link"] || [elementName isEqualToString:@"channel"] || [elementName isEqualToString:@"rss"]  || [elementName isEqualToString:@"description"]  || [elementName isEqualToString:@"ttl"]) 
		return;
	
    if (currentElementValue != nil && dictEvent != nil)
    {
        NSString *trimmedString = [currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [dictEvent setObject:trimmedString forKey:elementName];
    }
    else
    {
        [dictEvent setObject:@"" forKey:elementName];
    }
	[currentElementValue release];
	currentElementValue = nil;
}

- (void) dealloc {
	[currentElementValue release];
	[super dealloc];
}
@end
