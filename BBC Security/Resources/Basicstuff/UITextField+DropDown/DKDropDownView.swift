//
//  DKDropDownView.swift
//  Masakin
//
//  Created by DK on 14/11/16.
//
//

import UIKit
import LETimeIntervalPicker

extension UITextField {
    func setDropDown(delegate:DropDowndelegate?  = nil, array : NSMutableArray) {
        let dropDownView = Bundle.main.loadNibNamed("DKDropDownView", owner: nil, options: nil)?[0] as! DKDropDownView
        dropDownView.datePicker.isHidden = true
        dropDownView.pickerView.isHidden = false
        dropDownView.delegate = delegate
        dropDownView.inputArray = array
        dropDownView.textfield = self
        self.tintColor = UIColor.clear
        self.inputView = dropDownView
    }
    func setDatePicker(delegate:DropDowndelegate? = nil,mode:UIDatePickerMode? = .date ,formatte:String,date:Date? = nil,max:Date? = nil,min:Date? = nil) {
        let dropDownView = Bundle.main.loadNibNamed("DKDropDownView", owner: nil, options: nil)?[0] as! DKDropDownView
        dropDownView.datePicker.isHidden = false
        dropDownView.pickerView.isHidden = true
        dropDownView.datePicker.maximumDate = max
        dropDownView.datePicker.minimumDate = min
        dropDownView.datePicker.datePickerMode = mode!
        dropDownView.formate = formatte
        dropDownView.delegate = delegate
        dropDownView.textfield = self
        dropDownView.datePicker.locale = Locale(identifier: "en_US_POSIX")
        if date != nil {
            dropDownView.datePicker.date = date!
        }
        self.tintColor = UIColor.clear
        self.inputView = dropDownView
    }
    func setDatePicker(delegate:DropDowndelegate? = nil) {
        let dropDownView = Bundle.main.loadNibNamed("DKDropDownView", owner: nil, options: nil)?[1] as! TimePickerView
        dropDownView.delegate = delegate
        dropDownView.textfield = self
        self.tintColor = UIColor.clear
        self.inputView = dropDownView
    }
    
}

@objc protocol DropDowndelegate {
    @objc optional func textFieldDidPressDone(textField:UITextField,index:Int)
    @objc optional func textFieldDidPressDone(textField:UITextField,date:Date)
    @objc optional func textFieldDidPressCancle(textField:UITextField)
    
    @objc optional func textFieldDidPressDone(textField:UITextField,timeInterval:TimeInterval)
}

class DKDropDownView: UIView,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var datePicker: UIDatePicker!
    var delegate:DropDowndelegate?
    public var inputArray:NSMutableArray! = NSMutableArray()
    public var textfield:UITextField!
    private var selectedValue:String = ""
    var formate:String? = "yyyy-MM-dd"
    @IBOutlet var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.pickerView.delegate = self
//        self.datePicker.addTarget(self, action: #selector(datePickerValueChange(_:)), for: .valueChanged)
    }
    
//    func datePickerValueChange(_ sender:Any) {
//        
//        
//    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return inputArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        
        
        if self.textfield.tag == 1 || self.textfield.tag == 2 {
            
            let dict:NSMutableDictionary = (inputArray.object(at: row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            return dict.value(forKey: "Name") as? String
        }
       
        
        return inputArray.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        
        if self.textfield.tag == 1 || self.textfield.tag == 2 {
            
            let dict:NSMutableDictionary = (inputArray.object(at: row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            selectedValue = (dict.value(forKey: "Id") as? String)!
        }
        if self.textfield.tag == 3  {
            
            selectedValue = inputArray.object(at: row) as! String
        }
        
       // selectedValue = (inputArray.object(at: row) as? String)!
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        textfield.resignFirstResponder()
        
        if !self.datePicker.isHidden {
            textfield.resignFirstResponder()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = formate
            
            //textfield.text = dateFormatter.string(from: self.datePicker.date)
            if delegate != nil {
                self.delegate!.textFieldDidPressDone!(textField: textfield, date: self.datePicker.date)
            }
        }
        
        if !self.pickerView.isHidden {
            let selectedIndex:Int = self.pickerView.selectedRow(inComponent: 0)
            
            //selectedValue = (self.inputArray.object(at: selectedIndex) as? String)!
            if self.textfield.tag == 3  {
                textfield.text = inputArray.object(at: selectedIndex) as? String
            }
            else
            {
                let dict:NSMutableDictionary = (inputArray.object(at: selectedIndex) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                textfield.text = dict.value(forKey: "Id") as? String
            }
            
            
            if delegate != nil {
                self.delegate!.textFieldDidPressDone!(textField:textfield,index:selectedIndex)
            }
        }
    }
    @IBAction func btnCancleAction(_ sender: Any) {
        textfield.resignFirstResponder()
        if delegate != nil {
            self.delegate?.textFieldDidPressCancle!(textField: textfield)
        }
    }
}

class TimePickerView:UIView {
    @IBOutlet var timePicker: LETimeIntervalPicker!
    var delegate:DropDowndelegate?
    public var textfield:UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        self.datePicker.addTarget(self, action: #selector(datePickerValueChange(_:)), for: .valueChanged)
    }
    @IBAction func btnDoneAction(_ sender: Any) {
        textfield.resignFirstResponder()
        if delegate != nil {
            self.delegate?.textFieldDidPressDone!(textField: self.textfield, timeInterval: self.timePicker.timeInterval)
        }
    }
    @IBAction func btnCancleAction(_ sender: Any) {
        textfield.resignFirstResponder()
        if delegate != nil {
            self.delegate?.textFieldDidPressCancle!(textField: textfield)
        }
    }
    
}
