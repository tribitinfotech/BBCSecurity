//
//  AppDelegate.swift
//  BBC Security
//
//  Created by Jitendra bhadja on 26/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//
//54    119    108

import UIKit
import HockeySDK
import MicrosoftAzureMobile
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        print("BBC")
        
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            
            center.requestAuthorization(options: [.alert, .badge, .sound], completionHandler: { (granted:Bool, error:Error?) in
                if(error == nil){
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        BITHockeyManager.shared().configure(withIdentifier: "29c2e824e21b4a36ba83977156534d42")
        BITHockeyManager.shared().start()
        BITHockeyManager.shared().authenticator.authenticateInstallation()
         BITHockeyManager.shared().testIdentifier()
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        if UserDefaults.standard.bool(forKey: "firstTime") == false
        {
            UserDefaults.standard.set(true, forKey: "firstTime")
            UserDefaults.standard.setValue("", forKey: "userId")
            UserDefaults.standard.synchronize()
        }
        
        // Override point for customization after application launch.
        return true
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = String(format: "%@", deviceToken as CVarArg).trimmingCharacters(in: CharacterSet(charactersIn: "<>")).replacingOccurrences(of: " ", with: "")
        print("token string ==>" + token)
        
        ClientManager.sharedClient.push.registerDeviceToken(deviceToken) { error in
            print("Error registering for notifications: ", error?.localizedDescription)
        }
        
        //Token.token = token
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        print(userInfo)
        
        let apsNotification = userInfo["aps"] as? NSDictionary
        let apsString       = apsNotification?["alert"] as? String
        
        let alert = UIAlertController(title: "Alert", message: apsString, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            print("OK")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { _ in
            print("Cancel")
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        var currentViewController = self.window?.rootViewController
        while currentViewController?.presentedViewController != nil {
            currentViewController = currentViewController?.presentedViewController
        }
        
        currentViewController?.present(alert, animated: true) {}
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        NSLog("Handle push from background or closed" )
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        NSLog("handle notification for backgound :- %@", response.notification.request.content.userInfo)
        
        let apsNotification = response.notification.request.content.userInfo["aps"] as? NSDictionary
        let apsString       = apsNotification?["alert"] as? String
        
        let alert = UIAlertController(title: "Alert", message: apsString, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            print("OK")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { _ in
            print("Cancel")
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        var currentViewController = self.window?.rootViewController
        while currentViewController?.presentedViewController != nil {
            currentViewController = currentViewController?.presentedViewController
        }
        
        currentViewController?.present(alert, animated: true) {}
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userNavigate"), object: response.notification.request.content.userInfo)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        NSLog("Handle push from foreground" )
        // custom code to handle push while app is in the
        NSLog("handle notification for foregound %@", notification.request.content.userInfo);
        
        let userInfo:NSDictionary = notification.request.content.userInfo as NSDictionary
        print(userInfo)
    
        let apsNotification = userInfo["aps"] as? NSDictionary
        let apsString       = apsNotification?["alert"] as? String
        
        let alert = UIAlertController(title: "Alert", message: apsString, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            print("OK")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { _ in
            print("Cancel")
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        var currentViewController = self.window?.rootViewController
        while currentViewController?.presentedViewController != nil {
            currentViewController = currentViewController?.presentedViewController
        }
        
        currentViewController?.present(alert, animated: true) {}
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshMessageDetail"), object: notification.request.content.userInfo)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshMessageList"), object: nil)
        
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

