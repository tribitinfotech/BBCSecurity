//
//  DocumentViewController.swift
//  BBC Security
//
//  Created by Jitendra bhadja on 28/07/18.
//  Copyright © 2018 Jitendra bhadja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView

class cellAids:UITableViewCell
{
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    
}

class DocumentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblFirstAid: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    var isFromFirstAiders:Bool?
    var strType:String?
    var strTitle:String = ""
    var strDocType:String = ""
    
    var dictUser:NSMutableDictionary = NSMutableDictionary()
    var arrFirstAids:NSMutableArray = NSMutableArray()
    var arrDOC:[NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SKActivityIndicator.spinnerColor(UIColor.init(red: 31.0/255.0, green: 31.0/255.0, blue: 31.0/255.0, alpha: 1.0))
        SKActivityIndicator.statusTextColor(UIColor.black)
        let myFont = UIFont(name: "AvenirNext-DemiBold", size: 18)
        SKActivityIndicator.statusLabelFont(myFont!)
        
        self.lblTitle.text = self.strTitle
        
        let dataProfile:NSData = UserDefaults.standard.object(forKey: "userData") as! NSData
        
        self.dictUser =  NSMutableDictionary.init(dictionary: NSKeyedUnarchiver.unarchiveObject(with: dataProfile as Data as Data) as! NSDictionary)
        
        print(self.dictUser)
        
        if self.isFromFirstAiders == true {
            
            self.getDocumentForFirstAider()
        }
        else
        {
            self.getDocument()
        }
        
        // Do any additional setup after loading the view.
    }

    //MARK:- Tableview delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.strDocType == "QHSE"  ||  self.strDocType == "TB"  ||  self.strDocType == "MS"{
            return self.arrDOC.count
        }
        else
        {
            return self.arrFirstAids.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell  = self.tblFirstAid.dequeueReusableCell(withIdentifier: "cellAids") as! cellAids
        
       
        
        if self.strDocType == "QHSE"  ||  self.strDocType == "TB"  ||  self.strDocType == "MS"{
            
            let dict:NSDictionary = self.arrDOC[indexPath.row]
            
            let dictName:NSDictionary = dict["Name"] as! NSDictionary
           
            cell.lblTitle.text = dictName["text"] as? String
            
            cell.btnCall.setImage(UIImage.init(named: "ic_download"), for: .normal)
            cell.btnCall.tag = indexPath.row
            cell.btnCall.addTarget(self, action: #selector(btnDownloadAction(_:)), for: .touchUpInside)
        }
        else
        {
            let dict:NSMutableDictionary = (self.arrFirstAids.object(at: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            
            cell.lblTitle.text = "\(dict.value(forKey: "Id") as! String)"
            
            cell.btnCall.setImage(UIImage.init(named: "call"), for: .normal)
            cell.btnCall.tag = indexPath.row
            cell.btnCall.addTarget(self, action: #selector(btnCallAction(_:)), for: .touchUpInside)
        }
        
        
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    //MARK:- Action zones
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Private method
    
    @objc func btnDownloadAction(_ sender:UIButton)
    {
        let btn:UIButton = sender
        
    }
    @objc func btnCallAction(_ sender:UIButton)
    {
        
        let btn:UIButton = sender
        
        let dict:NSMutableDictionary = (self.arrFirstAids.object(at: btn.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        
        guard let number = URL(string: "tel://\(dict.value(forKey: "Name") as! String)") else { return }
        UIApplication.shared.open(number)
        
    }
    
    func getDocument()
    {
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        /*let arrCustomers:[String] = [(((UserDefaults.standard.value(forKey: "customerId") as? String)?.replacingOccurrences(of: "-", with: ""))?.lowercased())!]
        
        var strCustomerId:String = (UserDefaults.standard.value(forKey: "customerId") as? String)!
        
        strCustomerId = strCustomerId.replacingOccurrences(of: "-", with: "")
        
        strCustomerId = strCustomerId.lowercased()
        
        let headers = [
            "Cache-Control": "no-cache"
        ]
        
        let strURL = "https://activedev.blob.core.windows.net/\(strCustomerId)?restype=container&comp=list&include=metadata&sv=2017-11-09&ss=bfqt&srt=sco&sp=rlap&se=2020-07-28T08:09:42Z&st=2018-07-28T00:09:42Z&spr=https&sig=L9bm231ZBTclb6XC%2FeX21PKi4Lu0E%2F2RCjct5ylHE2E%3D"
        
        print(strURL)
        
        let request = NSMutableURLRequest(url: NSURL(string: strURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
            } else
            {
                DispatchQueue.main.async {
                    
                    let httpResponse = response as? HTTPURLResponse
                    let theXML:String = String(decoding: data!, as: UTF8.self)
                    
                    var parseError: Error? = nil
                    var xmlDictionary = try! XMLReader.dictionary(forXMLString: theXML)
                    // Print the dictionary
                    print(xmlDictionary)
                    let dictEnumerationResults:NSDictionary = xmlDictionary["EnumerationResults"] as! NSDictionary
                    let dictBlobs:NSDictionary = dictEnumerationResults["Blobs"] as! NSDictionary
                    let arrBlob:NSArray = dictBlobs["Blob"] as! NSArray
                    
                    for item in arrBlob
                    {
                        let dict:NSDictionary = item as! NSDictionary
                        let dictMetadata:NSDictionary = dict["Metadata"] as! NSDictionary
                        let dictDocType:NSDictionary = dictMetadata["doctype"] as! NSDictionary
                        if dictDocType["text"] as! String == self.strDocType
                        {
                            self.arrDOC.append(dict)
                        }
                    }
                    SKActivityIndicator.dismiss()
                    self.tblFirstAid.reloadData()
                }
                
                
            }
        })
        
        dataTask.resume()*/
        
        var strCustomerId:String = (UserDefaults.standard.value(forKey: "customerId") as? String)!
        
        strCustomerId = strCustomerId.replacingOccurrences(of: "-", with: "")
        
        strCustomerId = strCustomerId.lowercased()
        
        let strURL = "https://activedev.blob.core.windows.net/\(strCustomerId)?restype=container&comp=list&include=metadata&sv=2017-11-09&ss=bfqt&srt=sco&sp=rlap&se=2020-07-28T08:09:42Z&st=2018-07-28T00:09:42Z&spr=https&sig=L9bm231ZBTclb6XC%2FeX21PKi4Lu0E%2F2RCjct5ylHE2E%3D"
        
        print(strURL)
        
        Alamofire.request(strURL, method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            SKActivityIndicator.dismiss()
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8)
            {
                let theXML:String = String(decoding: data, as: UTF8.self)
                print(theXML)
                
                var parseError: Error? = nil
                var xmlDictionary = try! XMLReader.dictionary(forXMLString: theXML)
                // Print the dictionary
                print(xmlDictionary)
                let dictEnumerationResults:NSDictionary = xmlDictionary["EnumerationResults"] as! NSDictionary
                let dictBlobs:NSDictionary = dictEnumerationResults["Blobs"] as! NSDictionary
                let arrBlob:NSArray = dictBlobs["Blob"] as! NSArray
                
                for item in arrBlob
                {
                    let dict:NSDictionary = item as! NSDictionary
                    let dictMetadata:NSDictionary = dict["Metadata"] as! NSDictionary
                    let dictDocType:NSDictionary = dictMetadata["doctype"] as! NSDictionary
                    if dictDocType["text"] as! String == self.strDocType
                    {
                        self.arrDOC.append(dict)
                    }
                }
                SKActivityIndicator.dismiss()
                self.tblFirstAid.reloadData()
            }
        }
        
        
        
    }
    
    func getDocumentForFirstAider()
    {
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.show("", userInteractionStatus: true)
        
        //let arrCustomers:[String] = [(((UserDefaults.standard.value(forKey: "customerId") as? String)?.replacingOccurrences(of: "-", with: ""))?.lowercased())!]
        
        let arrCustomers:[String] = [(UserDefaults.standard.value(forKey: "customerId") as? String)!]
        let strCustomerId:String = "[\(arrCustomers[0])]"
        
        let strURL:String = firstAiders + arrCustomers[0]
        
        print(strURL)
        
        var request = URLRequest(url: URL(string: strURL)!)
        request.httpMethod = "GET"
        
        print(self.dictUser.value(forKey: "authenticationToken") as! String)
        
        request.addValue(self.dictUser.value(forKey: "authenticationToken") as! String, forHTTPHeaderField: "X-ZUMO-AUTH")
        request.addValue("2.0.0", forHTTPHeaderField: "X-ZUMO-API")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                SKActivityIndicator.dismiss()
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                return
            }
            
            DispatchQueue.main.async {
                
                do {
                    let arrResult = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    print(arrResult)
                    
                    self.arrFirstAids = arrResult
                    
                    self.tblFirstAid.reloadData()
                    
                    
                    SKActivityIndicator.dismiss()
                    
                    // use jsonData
                } catch {
                    // report error
                }
            }
            
            
            
        })
        
        dataTask.resume()
        
        
    }
    
    //MARK:- Memory managment method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
